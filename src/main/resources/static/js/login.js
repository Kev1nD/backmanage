/**
 * Created by daikai on 2017/9/19.
 */
layui.use(['jquery','layer'], function(){
    var $ = layui.jquery,
        layer = layui.layer;

    $(function(){

        $("#create").click(function(){
            check_register();
            return false;
        })
        $("#login").click(function(){
            check_login();
            return false;
        })


        function check_login(){
            var name=$("#user_name").val();
            var pass=$("#password").val();

            $.post("/user/login",{name:name,password:pass},function (json) {
                if(json.status){
                    window.location.href = "/";
                }else{
                    layer.msg(json.msg);
                    $("#login_form").removeClass('shake_effect');
                    setTimeout(function(){
                        $("#login_form").addClass('shake_effect')
                    },1);
                }
            })

        }
        function check_register(){
            var name = $("#r_user_name").val();
            var pass = $("#r_password").val();
            if(name!="" && pass!="" ){

                $.post("/user/regist",{name:name,password:pass},function (json) {
                    if(json.status){
                        layer.msg("注册成功！请直接登录！",{icon:1});
                    }else{
                        layer.msg(json.msg,{icon:2});
                        $("#login_form").removeClass('shake_effect');
                        setTimeout(function(){
                            $("#login_form").addClass('shake_effect')
                        },1);
                    }
                })

            }else{
                $("#login_form").removeClass('shake_effect');
                setTimeout(function(){
                    $("#login_form").addClass('shake_effect')
                },1);
            }
        }




        $('.message a').click(function () {
            $('form').animate({
                height: 'toggle',
                opacity: 'toggle'
            }, 'slow');
        });






    })

});