/**
 * Created by daikai on 2017/9/18.
 */



layui.use(['element','layer','jquery'], function(){
    var element = layui.element;
    var $ = layui.jquery,layer = layui.layer;

    $(function(){
        $(".js-add-user").on("click",function(){
            layer.open({
                type:1,
                title:'新增用户',
                area:['500px', '300px'],
                content:'<div style="text-align: center;margin-top: 80px">' +
                            '<div style="margin-bottom: 10px;height: 20px">用户名:<input type="text" id="userName"/></div>' +
                            '<div style="margin-bottom: 10px;height: 20px">密码：<input type="text"  id="password"/></div>' +
                        '</div>',
                btn:['确定','取消'],
                btnAlign:'c',
                yes:function(index){
                    var name = $("#userName").val().trim(),
                        password = $("#password").val().trim();
                    $.post("/user/add",{name:name,password:password},function(json){
                        if(json.status){
                            window.location.reload();
                        }
                    })
                }


            })
        })




        //删除用户
        $(".js-del-user").on("click",function(){
            var useid = $(this).attr("data");
            layer.confirm("确定删除该用户吗？",function(){
                $.post("/user/delete/"+useid,{},function(json){
                    if(json.status){
                        window.location.reload();
                    }
                })
            })
        })


        $(".js-edit-user").on("click",function(){
            var useid = $(this).attr("data");
            layer.open({
                type:1,
                title:'新增用户',
                area:['500px', '300px'],
                content:'<div style="text-align: center;margin-top: 80px">' +
                '<div style="margin-bottom: 10px;height: 20px">用户名:<input type="text" id="userName"/></div>' +
                '<div style="margin-bottom: 10px;height: 20px">密码：<input type="text"  id="password"/></div>' +
                '</div>',
                btn:['确定','取消'],
                btnAlign:'c',
                yes:function(index){
                    var name = $("#userName").val().trim(),
                        password = $("#password").val().trim();
                    $.post("/user/update/"+useid,{name:name,password:password},function(json){
                        if(json.status){
                            window.location.reload();
                        }
                    })
                }


            })
        })







    })

});

