package com.dk.controller;

import com.dk.entity.User;
import com.dk.result.JsonResult;
import com.dk.service.UserService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by daikai on 2017/9/19.
 */
@RequestMapping("/user")
@RestController
public class UserController {

    @Autowired
    private User user;

    @Autowired
    private UserService userService;


    @RequestMapping(value = "/login",method = RequestMethod.POST)
    public JsonResult sigin(HttpServletRequest request,HttpSession session){
        String name = request.getParameter("name");
        String password = request.getParameter("password");
        user = userService.queryUserByName(name);
        if(user == null){
            return new JsonResult(false,"用户不存在！");
        }else{
            if(StringUtils.equals(user.getPassword(),password)){
               session.setAttribute("USER_ID",user.getId());
               return new JsonResult(true);
            }
            return  new JsonResult(false,"密码错误！");

        }

    }


    @RequestMapping(value = "/regist",method = RequestMethod.POST)
    public JsonResult regist(HttpServletRequest request,HttpSession session){
        String name = request.getParameter("name");
        String password = request.getParameter("password");
        user = userService.queryUserByName(name);
        if(user != null){
            return new JsonResult(false,"用户名已经被注册！");
        }else{
            user = new User();
            user.setName(name);
            user.setPassword(password);
            userService.add(user);
            return new JsonResult(true);
        }
    }

    @RequestMapping("/logout")
    public void logout(HttpServletResponse response, HttpSession session) throws IOException{
        session.setAttribute("USER_ID",null);
        response.sendRedirect("/login");
    }

    @RequestMapping(value = "/add",method = RequestMethod.POST)
    public JsonResult addUser(HttpServletRequest request,HttpServletResponse response){
        String name = request.getParameter("name");
        String password = request.getParameter("password");
        user.setName(name);
        user.setPassword(password);
        userService.add(user);
        return new JsonResult(true);
    }



    @ApiOperation(value = "删除用户",notes = "根据用户id来删除用户")
    @ApiImplicitParam(name="id",required = true,dataType = "Long")
    @RequestMapping(value = "/delete/{id}",method = RequestMethod.POST)
    public JsonResult deletedUser(@PathVariable Long id){
        userService.deleteById(id);
        return new JsonResult(true);
    }



    @RequestMapping(value = "/update/{id}",method = RequestMethod.POST)
    public JsonResult updateUser(@PathVariable Long id,HttpServletRequest request){
        String name = request.getParameter("name");
        String password = request.getParameter("password");
        user.setName(name);
        user.setPassword(password);
        userService.update(user);
        return new JsonResult(true);
    }

}
