package com.dk.controller;

import com.dk.entity.User;
import com.dk.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

/**
 * Created by daikai on 2017/9/15.
 */

@Controller
public class Application {


    @Autowired
    private UserService userService;

    @Autowired
    private  User user;


    @ModelAttribute("curUser")
    public User currentUser(HttpSession session){
        Long userId = (Long)session.getAttribute("USER_ID");
        if (userId == null){
            return null;
        }
        user = userService.queryUserById(userId);
        return user;
    }


    @RequestMapping("/")
    public ModelAndView index(){
        ModelAndView modelAndView = new ModelAndView("index");
        return modelAndView;
    }


    @RequestMapping("/login")
    public String login(ModelMap modelMap){
        return "login";
    }

    @RequestMapping("/users")
    public String userList(ModelMap modelMap){
        List<User> users = userService.queryUserList(null);
        modelMap.put("users",users);
        return "users";
    }








}
