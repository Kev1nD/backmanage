package com.dk.result;

/**
 * Created by daikai on 2017/9/19.
 */
public class JsonResult {

    public boolean status;

    public String msg;

    public JsonResult(){

    }

    public JsonResult(boolean status){
        this.status = status;
    }

    public JsonResult(boolean status,String msg){
        this.status = status;
        this.msg = msg;
    }


}
