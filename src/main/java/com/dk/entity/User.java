package com.dk.entity;

import org.springframework.stereotype.Component;

/**
 * Created by daikai on 2017/9/13.
 */


@Component("user")
public class User {

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public Long getId(){
        return id;
    }

    private  Long id;

    private  String name;


    private  String password;


    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }





}
