package com.dk.aop;

import com.dk.controller.Application;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

/**
 * Created by daikai on 2017/9/19.
 */
@Aspect
@Component
public class WebLogAspect {


    private  Logger logger = LoggerFactory.getLogger(WebLogAspect.class);


    @Pointcut("execution(* com.dk.controller..*.*(..))")
    public void LogAspect(){

    }




    @Before("LogAspect()")
    public void doBefore(JoinPoint joinPoint){
        ServletRequestAttributes attributes = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
        if(attributes != null){
            HttpServletRequest httpServletRequest = attributes.getRequest();
            logger.info("当前请求地址:"+httpServletRequest.getRequestURL().toString());
            logger.info("METHOD:"+httpServletRequest.getMethod());
            logger.info("ip:"+httpServletRequest.getRemoteAddr());
            logger.info("CLASS_METHOD:"+joinPoint.getSignature().getDeclaringTypeName()+"."+joinPoint.getSignature().getName());
            logger.info("ARGS:"+ Arrays.toString(joinPoint.getArgs()));
        }
    }



    @AfterReturning(returning = "ret", pointcut = "LogAspect()")
    public void doAfterReturning(Object ret){
        logger.info("返回值:"+ret);

    }


}
