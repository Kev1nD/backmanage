package com.dk.service.impl;

import com.dk.dao.UserMapper;
import com.dk.entity.User;
import com.dk.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by daikai on 2017/9/13.
 */



@Service
public class UserServiceImpl implements UserService{

    @Autowired
    UserMapper mapper;


    @Override
    public int add(User user) {
       return this.mapper.add(user);
    }

    @Override
    public int update(User user) {
        return this.mapper.update(user);
    }

    @Override
    public int deleteByIds(Integer[] ids) {
        return 0;
    }

    @Override
    public User queryUserById(Long id) {
        return this.mapper.queryUserById(id);
    }

    @Override
    public User queryUserByName(String name) {
        return this.mapper.queryUserByName(name);
    }


    @Override
    public List<User> queryUserList(Map<String,Object> params){
        return this.mapper.queryUserList(params);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteById(id);
    }
}
