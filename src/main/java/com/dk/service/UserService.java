package com.dk.service;

import com.dk.entity.User;

import java.util.List;
import java.util.Map;

/**
 * Created by daikai on 2017/9/13.
 */
public interface UserService {


    int add(User user);

    int update(User user);


    int deleteByIds(Integer[] ids);

    User queryUserById(Long id);

    User queryUserByName(String name);

    List<User> queryUserList(Map<String,Object> params);

    int deleteById(Long id);
}
